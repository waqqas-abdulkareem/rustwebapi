mod api;
mod models;
mod data;
#[macro_use] 
extern crate nickel;
extern crate rustc_serialize;

#[macro_use(bson, doc)]
extern crate bson;
extern crate mongodb;

// rustc_serialize
use bson::oid::ObjectId;
use rustc_serialize::json::{Json, ToJson};
// Nickel
use nickel::{Nickel, JsonBody, HttpRouter, MediaType};
use nickel::status::StatusCode::{self};
// MongoDB
use mongodb::{Client, ThreadedClient};
// bson
// project
use api::result::{ApiResult};
use models::user::User;
use data::{MongoRepository};
use std::sync::Arc;

const MONGOD_PORT : u16 = 27017;

fn main() {

    let mut server = Nickel::new();
    let mut router = Nickel::router();

    let client = Client::connect("127.0.0.1",MONGOD_PORT).unwrap();
    let mongodb = MongoRepository::new(client,"rust-users".into());

    {
        let mongodb = mongodb.clone();
        router.get("/users/:id", middleware!(|request, response|{
            let opt_user =  match mongodb.find_by_id("users",request.param("id").unwrap_or("")){
                Ok(opt_user)=>opt_user,
                Err(e)=>return response.send(ApiResult::Err(StatusCode::InternalServerError,format!("{}",e))),
            };
            match opt_user {
                Some(user) => ApiResult::Ok(StatusCode::Ok, user),
                None => ApiResult::Err(StatusCode::NotFound, Json::Null)
            } 
        }));
    }

    {
        let mongodb = mongodb.clone();
        router.get("/users",middleware!(|_,mut response|{
            let array : Json = try_with!(response, {mongodb.find_as_json("users",None,None)
                .map_err(|e| (StatusCode::InternalServerError, e))});

            ApiResult::Ok(StatusCode::Ok,array)     
        }));
    }

    {
        let mongodb = mongodb.clone();
        router.post("/users",middleware!(  |request ,mut response |{
            let user = match request.json_as::<User>() {
                Ok(user)=>user,
                Err(e)=> return response.send(ApiResult::Err(StatusCode::BadRequest,format!("{}",e))),
            };
            let id = try_with!(response, {mongodb.insert("users",user)
                .map_err(|e| (StatusCode::InternalServerError, e))});

            ApiResult::Ok(StatusCode::Ok,id.to_json())

        }));
    }


    {
        let mongodb = mongodb.clone();
        router.put("/users/:id",middleware!(|request,mut response|{

            /*param returns &str which is a pointer into the immutable request object.
            * This causes a problem because the next line request.json_as::<User>
            * requires a mutable reference.
            * The compiler complains: cannot borrow *request as mutable because it is also borrowed as immutable
            * Therefore, id needs to copy the data from request by calling to_owned()
            *
            * The lesson to lear is that if you encounter this error message:
            * 'cannot borrow *request as mutable because it is also borrowed as immutable'
            * make sure that you do not have a variable that points into the immutable object.
            * I blindly tried to fix this problem using scopes.
            *
            * When Rust doesn't work, I hate the language.
            * When I find out why it doesn't work, I love the language!!
            */
            let id = request.param("id").unwrap_or("").to_owned();
            let user = match request.json_as::<User>(){
                 Ok(user)=>user,
                 Err(e)=>return response.send(ApiResult::Err(StatusCode::InternalServerError,format!("{}",e))),
             };

            let update_id = match mongodb.update_by_id("users",&id,user) {
                Ok(update_id) => return response.send(ApiResult::Ok(StatusCode::Ok,update_id.to_json())),
                Err(e) => return response.send(ApiResult::Err(StatusCode::InternalServerError,format!("{}",e))),
            };    

        }));
    }

    {
        let mongodb = mongodb.clone();
        router.delete("/users/:id",middleware!(|request,mut response|{ 
            match mongodb.delete("users",request.param("id").unwrap_or("")){
                Ok(count)=>return response.send(ApiResult::Ok(StatusCode::Ok,count)),
                Err(e)=>return response.send(ApiResult::Err(StatusCode::InternalServerError,format!("{}",e))),
            };
        }));
    }

    server.utilize(router);
    server.listen("127.0.0.1:6767");
}
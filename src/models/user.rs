extern crate rustc_serialize;

use std::collections::BTreeMap;
use rustc_serialize::json::{Json, ToJson};
#[derive(RustcDecodable, RustcEncodable)]
pub struct User {
    firstname: String,
    lastname: String,
    email: String
}

impl ToJson for User{
    fn to_json(&self)->Json{
        let mut obj = BTreeMap::new();
        obj.insert("firstname".into(),self.firstname.to_json());
        obj.insert("lastname".into(),self.lastname.to_json());
        obj.insert("email".into(),self.email.to_json());
        Json::Object(obj)
    }
}
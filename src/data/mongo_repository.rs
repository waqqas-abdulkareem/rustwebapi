extern crate rustc_serialize;
extern crate mongodb;

use std::sync::Arc;

use mongodb::{Client, ThreadedClient};
use mongodb::db::ThreadedDatabase;
use mongodb::error::Error::{self,WriteError,ArgumentError};
use mongodb::coll::options::FindOptions;
// bson
use bson::{Bson, Document};
use bson::oid::ObjectId;

use rustc_serialize::json::{Json, ToJson, DecodeResult};
use rustc_serialize::Decodable;
use rustc_serialize::json::decode;

pub struct MongoRepository{
	client: Client,
	db: String,
}


impl MongoRepository{

	pub fn new(client : Client, db: String)->Arc<MongoRepository>{
		return Arc::new(MongoRepository{
			client: client,
			db: db,
		})
	}

	pub fn insert<T: Sized + ToJson>(&self, coll: &str, data: T)->Result<Json,Error>{
		let collection = self.client.db(&self.db).collection(coll);
		let bson_doc = match Bson::from_json(&data.to_json()){
			Bson::Document(doc)=>doc,
			_ => return Err(ArgumentError("Json is not ordered document".into()))
		};
		let insert_result  = try!(collection.insert_one(bson_doc,None));
		if let Some(write_except) = insert_result.write_exception{
			return Err(WriteError(write_except));
		}
		Ok(insert_result.inserted_id.unwrap().to_json())
	}

	pub fn find_as_json(&self, coll: &str, query: Option<Document>, opts: Option<FindOptions>)->Result<Json,Error>{
		let collection = self.client.db(&self.db).collection(coll);
		let cursor = try!(collection.find(query, opts));
		let mut vec : Vec<Json> = vec![];
        for result in cursor.filter(|d| d.is_ok()){
        	vec.push(Bson::Document(result.unwrap()).to_json());
        }
        Ok(Json::Array(vec))
	}

	pub fn find_by_id(&self, coll: &str, id: &str)->Result<Option<Json>,Error>{
		let collection = self.client.db(&self.db).collection(coll);
		let oid = try!(ObjectId::with_string(id).map_err(|_| ArgumentError("invalid id".into())));
		if let Some(doc) = try!(collection.find_one(Some(doc!{"_id"=>oid}),None)){
		 	//returning the object removed the oid field.
		 	Ok(Some(Bson::Document(doc).to_json()))
		}else{
			Ok(None)
		}
	}

	pub fn update_by_id<T:Sized+ToJson>(&self, coll: &str, id: &str, data: T)->Result<Json,Error>{
		let collection = self.client.db(&self.db).collection(coll);
		let oid = try!(ObjectId::with_string(id).map_err(|_| ArgumentError("invalid id".into())));
		let bson_doc = match Bson::from_json(&data.to_json()){
			Bson::Document(doc)=>doc,
			_ => return Err(ArgumentError("Json is not ordered document".into()))
		};
		let update_result = try!(collection.update_one(doc!{"_id"=>oid},bson_doc,None));
		if let Some(write_exception) = update_result.write_exception{
			return Err(WriteError(write_exception));
		}
		Ok(update_result.upserted_id.unwrap().to_json())
	}

	pub fn delete(&self, coll: &str, id: &str)->Result<i32,Error>{
		let collection = self.client.db(&self.db).collection(coll);
		let oid = try!(ObjectId::with_string(id).map_err(|_| ArgumentError("invalid id".into())));
		let delete_result = try!(collection.delete_one(doc!{"_id"=>oid},None));
		if let Some(write_except) = delete_result.write_exception{
			return Err(WriteError(write_except));
		}
		Ok(delete_result.deleted_count)
	}

	#[allow(dead_code)]
	pub fn bson_decode<T: Decodable>(doc : Document)->DecodeResult<T>{
		Ok(try!(MongoRepository::json_decode(Bson::Document(doc).to_json())))
	}

	#[allow(dead_code)]
	pub fn json_decode<T: Decodable>(json: Json)->DecodeResult<T>{
		Ok(try!(decode(&format!("{}",json.pretty()))))
	}
}
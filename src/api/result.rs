extern crate nickel;
extern crate rustc_serialize;

// Nickel
use nickel::{Response, Responder, MiddlewareResult, MediaType};
use nickel::status::StatusCode::{self};

// rustc_serialize
use rustc_serialize::json::{Json, ToJson};

use std::collections::BTreeMap;

pub enum ApiResult<T : Sized + ToJson>{
    Ok(StatusCode, T),
    Err(StatusCode,T)
}

pub trait ToApiResult<T: Sized + ToJson>{
    fn to_api_result(&self)->ApiResult<T>;
}

impl<D,T> Responder<D> for ApiResult<T> where T: Sized + ToJson {

    fn respond<'a>(self, mut response: Response<'a, D>) -> MiddlewareResult<'a, D> {
        
        response.set(MediaType::Json);

        let mut d = BTreeMap::new();

        match self {
            ApiResult::Ok(code,data)=>{
                response.set(code);
                d.insert("Data".to_string(),data.to_json());
            }
            ApiResult::Err(code,err)=>{
                response.set(code);
                d.insert("Error".to_string(),err.to_json());
            }
        }

        response.send(Json::Object(d))
    }
}

impl ToApiResult<Json> for (StatusCode,Json){

    fn to_api_result(&self)->ApiResult<Json>{
        if self.0.is_success(){
            ApiResult::Ok(self.0,self.1.clone())
        }else{
            ApiResult::Err(self.0,self.1.clone())
        }
    }
}

impl ToApiResult<Json> for (StatusCode, String){

    fn to_api_result(&self)->ApiResult<Json>{
        if self.0.is_success(){
            ApiResult::Ok(self.0,self.1.to_json())
        }else{
            ApiResult::Err(self.0,self.1.to_json())
        }
    }
}

extern crate rustc_serialize;
use rustc_serialize::json::{Json, ToJson};
use api::result::{ApiResult};
use nickel::status::StatusCode::{self};
use models::user::User;
use data::{MongoRepository};
use std::sync::Arc;

pub fn get_by_id(mongodb: &Arc<MongoRepository>, id: &str)->ApiResult<Json>{
    let opt_user =  match mongodb.find_by_id("users",id){
        Ok(opt_user)=>opt_user,
        Err(e)=>return ApiResult::Err(StatusCode::InternalServerError,format!("{}",e).to_json()),
    };
    match opt_user {
        Some(user) => ApiResult::Ok(StatusCode::Ok, user),
        None => ApiResult::Err(StatusCode::NotFound, Json::Null)
    } 
}

pub fn list(mongodb: &Arc<MongoRepository>)->ApiResult<Json>{
	match mongodb.find_as_json("users",None,None){
		Ok(array)=>ApiResult::Ok(StatusCode::Ok,array),
		Err(e)=>ApiResult::Err(StatusCode::InternalServerError,format!("{}",e).to_json())
	}
}